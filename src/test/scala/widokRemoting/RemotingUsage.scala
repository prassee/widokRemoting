package widokRemoting

import skanban.HTTPMethods
import scala.util.Success
import scala.scalajs
  .concurrent
  .JSExecutionContext
  .Implicits
  .runNow
import skanban.HTTP
import scala.util.Success

object RemotingUsage extends App {

  val svcResp = HTTP.get("http://localhost:8080/service")
  svcResp.onComplete {
    case Success(e) => println(e)
  }
}