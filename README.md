### widokRemoting
a simple helper  on top of scala.js Ajax class to support remoting in Widok framework


### Usage

```
import skanban.HTTPMethods
import scala.util.Success
import scala.scalajs
  .concurrent
  .JSExecutionContext
  .Implicits
  .runNow
  
  val svcResp = HTTP.get("http://localhost:8080/service")
  svcResp.onComplete {
    case Success(e) => println(e)
  }
  
```

